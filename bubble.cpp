// CREATED BY MUHAMMAD HANAN ASGHAR
// PYTHONIST
#include <iostream>
using namespace std;
class BubbleSort{
  private:
  int *arr;
  int arraySize,temp;
  public:
  BubbleSort(int size){
    arr = new int[size];
    arraySize = size;
  }
  void Insert(){
    for(int i=0;i<arraySize;i++){
      cout<<"Enter Data - "<<i<<" : ";
      cin>>arr[i];
    }
  }
  void Display(){
    for(int i=0;i<arraySize;i++){
      cout<<arr[i]<<" - ";
    }
  }
  void Sort(){
    for(int i = 0; i < (arraySize-1);i++){
      for(int j = 0;j<(arraySize - i) - 1;j++){
            if(arr[j] < arr[j+1]){
              temp = arr[j];
              arr[j] = arr[j+1];
              arr[j+1] = temp;
            }
      }
    }
  }
};

int main() {
  BubbleSort b(5);
  b.Insert();
  cout<<endl;
  b.Display();
  cout<<endl;
  b.Sort();
  b.Display();
}